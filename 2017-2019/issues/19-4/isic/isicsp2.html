﻿<!doctype html>  
 <html>  
 <head>  
 <title>Visual and creative methods and quality in information behaviour research</title>  
<meta charset="utf-8">
 <link href="../../IRstyle3.css" rel="stylesheet" media="screen" title="serif">  
<link rel="alternate stylesheet" type="text/css" media="screen" title="sans" href="../../IRstylesans.css">  
 
 <!--Enter appropriate data in the content fields-->  
 <meta name="dcterms.title" content="Visual and creative methods and quality in information behaviour research">  
 <meta name="author" content="Melanie Benson, Andrew Cox">  
 <meta name="dcterms.subject" content="Give a brief description of your paper">  
 <meta name="description" content="The purpose of the paper is to explore the ways in which visual and creative methods can increase the quality of qualitative research in information behaviour. The paper uses Tracy's eight criteria of quality in qualitative research as a framework within which to explore the precise strengths and weaknesses of visual and creative methods. Visual and creative methods have the potential to create credible and resonant information behaviour research. There may be issues around rigour, ethics and coherence. Worthiness of the research or significance of the contribution do not seem to be so clearly linked to use of these methods. Particularly for the study of aspects of affective and everyday information behaviour, and the information behaviour of socially marginal groups, visual and creative methods are promising approaches.
">  
 <meta name="keywords" content="information behaviour, information practice, research methodology, visual and creative methods">
 
 
 <!--leave the following to be completed by the Editor-->  
 <meta name="robots" content="all">  
 <meta name="dcterms.publisher" content="Professor T.D. Wilson">
 <meta name="dcterms.type" content="text">  
 <meta name="dcterms.identifier" content="ISSN-1368-1613">  
 <meta name="dcterms.identifier" content="http://InformationR.net/ir/19-4/isic/isicsp2.html">  
 <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/19-4/infres194.html">  
 <meta name="dcterms.format" content="text/html">  <meta name="dc.language" content="en">  
 <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">  
 <meta name="dcterms.issued" content="2014-12-15"> 
  <meta name="geo.placename" content="global">  

 
  

</head> 
  
 <body> 
<header>
 <img height="45" alt="header" src="../../mini_logo2.gif" width="336"><br>
<span style="font-size: medium; font-variant: small-caps; font-weight: bold;">vol. 19  no. 4, December, 2014</span>
<br><br> 
<div class="button">
 <ul> 
 <li><a href="../infres194.html">Contents</a> |  </li> 
 <li><a href="../../iraindex.html">Author index</a> |  </li> 
 <li><a href="../../irsindex.html">Subject index</a> |  </li> 
 <li><a href="../../search.html">Search</a> |  </li> 
 <li><a href="../../index.html">Home</a> </li> 
 </ul>
 </div>
 <hr>
 </header>
 <article>
 <h1>Visual and creative methods and quality in information behaviour research</h1>  <br>  
 <div style="margin-left: 5%; margin-right:10%;"> 
 <h4><a href="#author">Melanie Benson and Andrew Cox</a><br>
 Information School, University of Sheffield, Sheffield S10 2TN, U.K.</h4>  
 </div>  
 <br> 

 <form>  <fieldset>  <legend>Abstract </legend>
<blockquote>
 <strong>Introduction.</strong> The purpose of the paper is to explore the ways in which visual and creative methods can increase the quality of qualitative research in information behaviour.<br>
 <strong>Method.</strong> The paper uses Tracy's eight criteria of quality in qualitative research as a framework within which to explore the precise strengths and weaknesses of visual and creative methods. <br>
 <strong>Analysis and results.</strong> Visual and creative methods have the potential to create credible and resonant information behaviour research. There may be issues around rigour, ethics and coherence. Worthiness of the research or significance of the contribution do not seem to be so clearly linked to use of these methods. <br>
 <strong>Conclusions.</strong> Particularly for the study of aspects of affective and everyday information behaviour, and the information behaviour of socially marginal groups, visual and creative methods are promising approaches.
 </blockquote>  </fieldset>  </form> 
  <section>
 <br>    <br> 


 
 <h2>Introduction</h2> 
 
 <p>The two decades has seen a turn across the social sciences towards increased use of visual and creative research methods (<a href="#Pin07">Pink, 2007</a>). One such technique is photovoice (<a href="#Wan97">Wang & Burris, 1997</a>), but methods such as drawing, map making, collages or making models have also been used by a number of researchers (<a href="#Bag09">Bagnoli, 2009</a>; <a href="#Man10">Mannay, 2010</a>). Visual methods are seen as engaging and accessible ways to undertake and disseminate research; they aim to actively engage communities in the research process, and explain the results of research to the public better. They may help to gather data from difficult to reach communities, and their constructive nature deepens the interviewee's engagement with the themes and allows them to express what might be hard to articulate in words (<a href="#Bri07">Briden, 2007</a>; <a href="#Gau07">Gauntlett, 2007</a>). Outputs of visual research can include an exhibition of material produced, widening public awareness, and perhaps impacting on policy. The trend towards using visual methods has not been without its impact on information science (<a href="#Har06">Hartel, 2006</a>, <a href="#Har10">2010</a>; <a href="#Har11">Hartel & Thomson, 2011</a>; <a href="#Jul13">Julien, Given, & Opryshko, 2013</a>). However, visual methods seem yet to have become truly mainstream in information science or information behaviour research.</p>
 
 <p>While qualitative methods are the most common approach to studying informa-tion behaviour (<a href="#Fis09">Fisher & Julien, 2009</a>), quantitative methods and positivist standards of research quality remain the dominant paradigm across Information Science as a whole. It is therefore important to develop our explanation of how we ensure quality in qualitative research. Caution about adopting visual and creative methods may be due to the perception of risk associated with using new methods. While we acknowledge that the method itself cannot inherently ensure the quality of the research outcomes, this paper's contribution is to consider in what ways visual and creative methods in themselves can inherently enhance the quality of qualitative research in information behaviour and identify where their weaknesses as methods lie.</p> 
 
 <p>Whereas there is a degree of consensus about what are the attributes of good quantitative research, what makes qualitative research good is more controversial. Key concepts such as replication seem impossible to emulate with qualitative research (<a href="#Sea99">Seale, 1999</a>). Others such as reliability, validity, objectivity and generalisation must be relevant. A useful synthesis of criteria of quality in qualitative research has been  provided by Tracy (<a href="#Tra10">2010</a>). The contribution of this paper is to develop a systematic account of the strengths and weaknesses of creative and visual methods using her eight point framework.</p> 
 
 <p><strong>Tracy's eight criteria</strong><br>Tracy proposes that good qualitative research investigates a worthy topic, one that is "relevant, timely, significant, interesting or evocative" (<a href="#Tra10">Tracy, 2010</a>, p. 840). In this respect, photovoice aims to "make the familiar strange" (<a href="#Man10">Mannay, 2010</a>, p. 95), and has been praised for its ability to "discover the unexpected" (<a href="#Bri07">Briden, 2007</a>, p. 41); it may be a useful way to challenge assumptions. Tracy next suggests that good qualitative research has "rich rigour" in terms of complex use of theory, data collection, and analysis. Visual methods include extensive time in the field and rich complex data (<a href="#Bri07">Briden, 2007</a>), but the researcher cedes control over the quality of data that is produced, and such methods make more demands on participants. Methods of analysis of visual data, such as semiotics, is less widely understood in information science, and researchers often fall back on simply analysing accompanying text (<a href="#Cat09">Catalani & Minkler, 2009</a>). The increasing interdisciplinarity of information behaviour research (<a href="#Jul13">Julien, Given, & Opryshko, 2013</a>) may aid acceptance of new forms of visual analysis, however. A third criterion is "sincerity"; by this Tracy means that good quality research is self-reflexive, transparent, and honest about its aims, methods, biases, limitations, successes and failures. In general, there is no reason to think visual methods improve self-reflexivity. Photovoice places more attention on the participating community, displacing attention from the researcher's own identity. It is not always very transparent about the analytic methods used (<a href="#Cat09">Catalani & Minkler, 2009</a>). </p> 
 
 <p>Tracy's fourth criteria, credibility, refers to research that is trustworthy and plausible. Visual methods can provide concrete detail (<a href="#Har11">Hartel & Thomson, 2011</a>) or visual illustration and explication (<a href="#Bri07">Briden, 2007</a>) to aid "thick description". They may provide insight into tacit knowledge in information behaviour (<a href="#Bag09">Bagnoli, 2009</a>; <a href="#Har10">Hartel, 2010</a>). Tracy suggests the concept of crystallization to corroborate findings: the use of multiple data sources, researchers, and interpretations to create a complex view of reality. Photovoice enables the presentation of many different realities, but it may be better achieved by the use of mixed methods (Hartel & Thomson, <a href="#Har11">2011</a>). Multivocality contributes to credibility; hearing the voices of young people, for example, migrational individuals, in information behaviour research has been predicted as an area of growing concern (<a href="#Wil10">Wilson, 2010</a>). Member reflections, which are usually built into photovoice, may further credibility. The fifth of Tracy's criteria, the quality of 'resonance' refers to research that can "meaningfully reverberate and affect an audience" (<a href="#Tra10">Tracy, 2010</a>: 844). Images have a capacity to promote empathy with marginalised groups in ways that text does not, by being aesthetically pleasing, troubling or evocative. Knowledge of how and why the images were constructed can add an extra dimension of resonance (<a href="#McI03">McIntyre, 2003</a>; <a href="#Str04">Strack, Magill, & McDonagh, 2004</a>). Tracy's concept of resonance also refers to forms of generalisation; photographic or drawn material may have a special ability to achieve certain forms of transferability. </p> 
 
<p>Tracy states that good qualitative research makes a theoretically, heuristically, methodologically or practically significant contribution. Heuristic significance, the ability to move the reader to explore a topic further, is a possible outcome of visual research; such methods can develop curiosity and inspire further work are useful to impact upon multiple audiences. Photovoice has the potential to make a practical contribution (<a href="#Str04">Strack, Magill, & McDonagh, 2004</a>), but the impact of such projects at policy level has been mixed (<a href="#Cat09">Catalani & Minkler, 2009</a>; <a href="#Kwo08">Kwok & Ku, 2008</a>). Tracy argues that high quality qualitative research is ethical. Visual methods, as an unfamiliar approach, may cause some concern to procedural ethics reviewers. Anonymity, release of images, sensitivity to local cultural practices, privacy and copyright issues are all of concern with visual methods. However, in terms of relational ethics, photovoice methods may create trust and can make a sensitive topic easier to introduce and discuss (<a href="#McI03">McIntyre, 2003</a>; <a href="#Str04">Strack, Magill, & McDonagh, 2004</a>). "Meaningful coherence" is found in studies that align their design, data collection, and analysis with their theoretical framework and goals. This may be problematic in visual research (<a href="#Emm00">Emmison & Smith, 2000</a>). Using visual data when publishing the work may actually reduce coherence, if the images are not anchored by some interpreting text; if images are included in the wrong place, it can also be discordant.</p>

<p><strong>Discussion and conclusion</strong><br>Our analysis suggests that the greatest benefits of visual methods are in particular areas. Firstly, they are powerful in terms of providing thick description, capturing concrete detail and eliciting tacit understandings, a preoccupation of information behaviour research (<a href="#Bur10">Burnett & Erdelez, 2010</a>"). Such methods seem particularly relevant to eliciting data around the less easy to articulate side of information activities such as affective aspects or embodied knowing, and in the burgeoning area of everyday life information seeking among young people, migrational individuals and other marginal groups. A second strength is in resonance, prompting viewers to strongly identify with the research. It also seems plausible that the accessibility of visual methods would be one way to bring academic and practitioner research in information behaviour closer together, as called for by Julien, Given, and Opryshko (<a href="#Jul13">2013</a>). There are also some areas where visual methods seem innately more problematic. They do not lend themselves to affecting the issues of worthy topic or significant contribution. Visual methods are potentially ethically problematic. Yet the empowering capabilities of photovoice make it appealing morally. Greater understanding of visual analysis methods in information behaviour research is needed if visual material is to be itself analysed, not merely used to elicit more textual data. Transparency of process is also important and seems a little ne-glected in mainstream photovoice literature.</p>
 <p>As a general work, this paper can only take the discussion so far. Certain types of research question lend themselves to visual approaches, such as those with a strong focus on use of space. It could also be argued that the research paradigm choices of the researcher are critical to the relevance of visual methods. There is more to say about which versions of photovoice, for example, align with constructivist or critical para-digms. Tracy's (<a href="#Tra10">2010</a>) model concerns itself with the quality of research, rather than the processes used to achieve this quality. As a body of work using visual methods in information behaviour is built up, it will become much clearer how deeply visual and creative methods can contribute to the evolution of the subject.
</p>
  
</section>
<section class="refs">
 
 
 <form>  
 <fieldset>  
 <legend style="color: white; background-color: #5E96FD; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy;
 border-bottom: 1px solid navy; font-weight: bold;">References</legend>  
  
 
 <ul class="refs"> 
 
 <li id="Bag09">Bagnoli, A. (2009). Beyond the standard interview: the use of graphic elicitation and arts-based methods. <em>Qualitative Research, 9</em>(5), 547-570. </li>
 
 <li id="Bri07">Briden, J. (2007). Photo surveys: eliciting more than you knew to ask for. In N. F. Foster & S. Gibbons (Eds.), <em>Studying students: The undergraduate research project at the University of Rochester</em> (pp. 40-47). Chicago, IL: Association of College and Research Libraries.</li>
 
 <li id="Bur10">Burnett, G. & Erdelez, S. (2010). Forecasting the next 10 years in information behaviour research. <em>Bulletin for the American Society of Information Science and Technology, 36</em>(3), 44-48.</li> 
 
 <li id="Cat09">Catalani, C. & Minkler, M. (2009). Photovoice: A Review of the Literature in Health and Public Health. <em>Health Education Behavior, 37</em>(3), 424-451. </li>
 
 <li id="Emm00">Emmison, M. & Smith, P. (2000). Researching the visual: images, objects, contexts and interactions in social and cultural inquiry. London: SAGE.</li>
 
 <li id="Fis09">Fisher, K. E. & Julien, H. (2009). Information behavior. <em> Annual Review of Information Science & Technology</em>, 43, 1-73.</li>
 
 <li id="Gau07">Gauntlett, D. (2007). <em>Creative explorations :explorations: new approaches to identities and audiences</em>. London ;London; New York: Routledge.</li>
 
 <li id="Har06">Hartel, J. (2006). <em>Information activities and resources in an episode of gourmet cooking. Information Research, 12</em>(1). </li>
 
 <li id="Har10">Hartel, J. (2010). Managing documents at home for serious leisure: a case study of the hobby of gourmet cooking. [Case Study]. <em>Journal of Documentation, 66</em>(6), 847-874. </li>
 
 <li id="Har11">Hartel, J. & Thomson, L. (2011). Visual approaches and photography for the study of immediate information space. <em>Journal of the American Society for Information Science and Technology, 62</em>(11), 2214-2224.</li> 
 
 <li id="Jul13">Julien, H., Given, L. M. & Opryshko, A. (2013). Photovoice: A promising method for studies of individuals' information practices. <em>Library and Information Science Research, 35</em>(4), 257-263. </li>
 
 <li id="Kwo08">Kwok, J. Y. & Ku, H. (2008). Making habitable space together with female Chinese immigrants to Hong Kong: An interdisciplinary participatory action research project. <em>Action Research, 6</em>(3), 261-283. </li>
 
 <li id="Man10">Mannay, D. (2010). Making the familiar strange: can visual research methods render the familiar setting more perceptible? <em>Qualitative Research, 10</em>(1), 91-111. </li>
 
 <li id="McI03">McIntyre, A. (2003). Through the eyes of women: photovoice and participatory research as tools for reimagining place. <em>Gender, Place and Culture, 10</em>(1), 47-66.</li>
 
 <li id="Pin07">Pink, S. (2007). <em>Doing visual ethnography</em> (2 ed.). London: SAGE.</li>
 
 <li id="Sea99">Seale, C. (1999). <em>The quality of qualitative research</em>. London: SAGE.</li>
 
 <li id="Str04">Strack, R. W., Magill, C. & McDonagh, K. (2004). Engaging youth through photo-voice. <em>Health Promotion Practice, 5</em>(1), 49-58. </li>
 
 <li id="Tra10">Tracy, S. J. (2010). Qualitative quality: eight "big-tent" criteria for excellent qualitative research. <em>Qualitative Inquiry, 16</em>(10), 837-851. </li>
 
 <li id="Wan97">Wang, C. C. & Burris, M. A. (1997). Photovoice: concept, methodology, and use for participatory needs assessment. [Research Support, Non-U.S. Gov't]. <em>Health Education Behavior, 24</em>(3), 369-387. 
 
 </li><li id="Wil10">Wilson, T. D. (2010). Fifty years of information behaviour research. <em>Bulletin of the American Association of Information Science and Technology, 36</em>(3), 27-33.</li>

 
 
 
 
 
 </ul>  
 </fieldset>  
 </form> 
 
 
  
</section>
</article>
<br>
<section>
 
  
 
 
 <br>
 <div style="text-align:center;"> 
     



  
</div>
<hr>

 
  
 </section>
 <footer>

<hr> 
 
 <hr>
</footer>
     
 </body>  
 </html> 

