﻿<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Windows version 5.2.0">
    <meta charset="utf-8">
    <title>
      Tacit knowledge sharing: the determination of a methodological approach to explore the intangible
    </title>
    <link href="../../IRstyle4.css" media="screen" rel="stylesheet" title="serif"><!--Enter appropriate data in the content fields-->
    <meta content="Tacit knowledge sharing: the determination of a methodological approach to explore the intangible" name="dcterms.title">
    <meta content="Iris Buunk, Hazel Hall, Colin Smith" name="author">
    <meta content="Information behaviour" name="dcterms.subject">
    <meta content="Research design should take into account both (a) the specific nature of the object under scrutiny, and (b) approaches to its study in the past. This is to ensure that informed decisions are made regarding research design in future empirical studies. Here these factors are taken into account with reference to methodological choice for a doctoral study on tacit knowledge sharing, and the extent to tacit knowledge sharing may be facilitated by online tools. The larger study responds to calls for the two domains of knowledge management and human information behaviour to be considered together in terms of their research approaches and theory development" name="description">
    <meta content="knowledge management, research methods, tacit knowledge" name="keywords"><!--leave the following to be completed by the Editor-->
    <meta content="all" name="robots">
    <meta content="University of Borås" name="dcterms.publisher">
    <meta content="text" name="dcterms.type">
    <meta content="ISSN-1368-1613" name="dcterms.identifier">
    <meta content="http://InformationR.net/ir/22-1/isic/isicxx.html" name="dcterms.identifier">
    <meta content="http://InformationR.net/ir/22-1/infres221.html" name="dcterms.IsPartOf">
    <meta content="text/html" name="dcterms.format">
    <meta content="en" name="dc.language">
    <meta content="http://creativecommons.org/licenses/by-nd-nc/1.0/" name="dcterms.rights">
    <meta content="2017-03-15" name="dcterms.issued">
    <meta content="global" name="geo.placename">
  </head>
  <body>
    <header>
      <h4>
        vol. 22 no. 1, March, 2017
      </h4>
    </header>
    <article>
      <h1>
        Tacit knowledge sharing: the determination of a methodological approach to explore the intangible
      </h1><br>
      <h2 class="author">
        <a href="#author">Iris Buunk, Hazel Hall</a> and <a href="#author">Colin Smith</a>
      </h2><br>
      <blockquote>
        <strong>Introduction.</strong> Research design should take into account both (a) the specific nature of the object under scrutiny, and (b) approaches to its study in the past. This is to ensure that informed decisions are made regarding research design in future empirical studies. Here these factors are taken into account with reference to methodological choice for a doctoral study on tacit knowledge sharing, and the extent to tacit knowledge sharing may be facilitated by online tools. The larger study responds to calls for the two domains of knowledge management and human information behaviour to be considered together in terms of their research approaches and theory development.<br>
        <strong>Method.</strong> Relevant literature – both domain-specific (knowledge management) and general (research methods in social science) - was identified and analysed to identify the most appropriate approaches for an empirical study of tacit knowledge sharing.<br>
        <strong>Analysis.</strong> The analysis shows that there are a number of challenges associated with studying an intangible entity such as tacit knowledge. Quantitative, qualitative and mixed methods have been adopted in prior work on this theme, each with their own strengths and weaknesses.<br>
        <strong>Results.</strong> The analysis has informed a decision to adopt a research approach that deploys mixed methods for an inductive case study to extend knowledge of the influence of online tools on tacit knowledge sharing.<br>
        <strong>Conclusion.</strong> This work intends to open the debate on methodological choice and routes to implementation for studies that are subject to practical constraints imposed by the context in which they are situated.<br>
      </blockquote>
      <section>
        <br>
        <h2>
          Introduction
        </h2>
        <p>
          The focus of this paper is the presentation of a pragmatic methodology for a doctoral study in the domain of knowledge management. The study has a particular focus on tacit knowledge sharing. Approaches, theories and research from knowledge management have been discussed with human information behaviour in the past, and calls have been made for others to consider the two domains together (<a href="#hal07a">Halbwirth &amp; Olsson, 2007</a>). This paper is therefore of interest to a wide audience that includes human information behaviour researchers.
        </p>
        <p>
          The investigation in question will consider tacit knowledge sharing practices amongst public sector employees. It will contribute to closing a gap in knowledge related to organizational knowledge sharing with a focus on social media use (as discussed by <a href="#pan13">Panahi, 2013</a>, p. 380). It will thus address an identified need for the enhancement of understanding of the coexistence of information behaviour and knowledge sharing practices in virtual environments (<a href="#wid08">Widen-Wulff, Ek, Ginman, <em>et al.</em>, 2008</a>, p. 352). It has been noted that a better appreciation of information behaviour in such contexts can lead to the identification of means of addressing issues related to informal learning activities (<a href="#mil14a">Mills, Knezek &amp; Khaddage 2014</a>, p. 333). Amongst these are challenges related to tacit knowledge transfer.
        </p>
        <p>
          The larger study addresses the following research questions:
        </p>
        <ul>
          <li>How do social media facilitate the sharing of tacit knowledge between employees?
          </li>
          <li>To what extent do social media bring new capabilities in the sharing of tacit knowledge?
          </li>
          <li>Which situated factors may provide the appropriate context for using social media to enhance tacit knowledge sharing practices?
          </li>
        </ul>
        <p>
          The need for the development of robust methodological approaches for studies of tacit knowledge sharing has been articulated for some time. For example, Kane, Ragsdell and Oppenheim, highlighted this in 2005. More recently, Venkitachalam and Busch (<a href="#ven12">2012, p.360</a>) revisit this question. A particular problem is the practice of applying methodological approaches initially intended to study explicit knowledge to investigations of tacit knowledge, (<a href="#kan05">Kane <em>et al.</em>, 2005</a>, p.143). This is evident in a number of extant studies that take a positivist approach and generate theory in a deductive manner (<a href="#dur07">for example, Du, Ai &amp; Ren, 2007</a>; <a href="#hsu08">Hsu &amp; Lin, 2008</a>).
        </p>
        <p>
          For future work to contribute to research in the domain, it is important to take into account (1) the specific nature of the object of study (tacit knowledge) and (2) approaches to its study in the past (through the adoption of quantitative, qualitative, and mixed methods). This is to ensure that informed decisions are made regarding research design for future empirical studies. These two factors are explored with reference to the doctoral study described above, with a firm focus on tacit knowledge, knowledge sharing practice, and methodological choice.&nbsp;
        </p>
        <h2>
          Options for exploring the intangible
        </h2>
        <p>
          Research in the domain of knowledge management has a bias towards exploring knowledge in its explicit form, largely because explicit knowledge is more easily observed than tacit knowledge (<a href="#kan05">Kane <em>et al.</em>, 2005</a>, p. 142). It is also quantifiable, and therefore measureable (<a href="#vir10">Virtanen, 2010</a>, p.3). This is evident in much knowledge management research conducted in organizational settings (<a href="#dur07">for example, Du, Ai, &amp; Ren, 2007</a>; <a href="#hsu08">Hsu &amp; Lin, 2008</a>).
        </p>
        <p>
          Amongst the challenges of studying tacit knowledge and associated practices (such as tacit knowledge sharing) is the intangible nature of the object of study (<a href="#lin07">Lin, 2007</a>, p. 412; <a href="#des03">Desouza, 2003</a>, p. 86; <a href="#mil02">Miller, 2002</a>, p. 6), and complexities in comprehending - and then articulating - its facets (<a href="#non94">Nonaka, 1994</a>, p.24). Despite this, a number of researchers have explored tacit knowledge, often motivated by the recognition of the high value of tacit knowledge and desires to capitalise on this (<a href="#huy06">Huysman &amp; Wulf, 2006</a>; <a href="#nga09">Ngah, &amp; Jusoff, 2009</a>; <a href="#non94">Nonaka, 1994</a>).
        </p>
        <p>
          Some studies of tacit knowledge have taken a positivist approach through the adoption of quantitative methods - particularly in attempts to <em>model</em> tacit knowledge sharing, (<a href="#sal13">for example, Salleh, Chong, Ahmad, &amp; Ikhsan, 2013</a>; <a href="#tsa14">Tsai, 2014</a>). Others follow the tradition of interpretivist research, using qualitative techniques (<a href="#abd09">for example, Abdullah, Ingram, &amp; Welsh, 2009</a>; <a href="#scu13">Scully, Buttigieg, Fullard, Shaw, &amp; Gregson, 2013</a>). A number use mixed methods (<a href="#des03">for example, Desouza, 2003</a>; <a href="#gar07">Garcia-Perez, &amp; Mitra, 2007</a>). Presented below are the key characteristics of each approach as relevant to the question of research design for a study of tacit knowledge sharing practices amongst public sector employees.
        </p>
        <p>
          Typically studies of tacit knowledge which deploy quantitative methods are based on large-scale surveys, some of which make extensive use of the Likert scale (<a href="#bor13">for example, Borges, 2013</a>; <a href="#lin07">Lin, 2007</a>; <a href="#tsa14">Tsai, 2014</a>). Such studies have been criticised on the basis that these are often implementations initially designed for the study of explicit knowledge, and thus overlook the multidimensional nature of tacit knowledge (<a href="#kan05">Kane <em>et al.,</em> 2005</a>, p.143). A further deficiency of these studies is that their findings, and the models that emanate from them, remain untested (<a href="#hen99">for example, Hendricks, 1999</a>; <a href="#liy10">Li &amp; Zhang, 2010</a>). More important, however, is that a positivist approach to this domain of study fails to recognise that knowledge is socially constructed, embedded within, and inseparable from work activities and practice (<a href="#his13">Hislop, 2013, p. 31</a>). Thus positivist studies risk the production of findings that describe the <em>assets</em> generated from tacit knowledge (such as explicit knowledge in the form of information), rather than develop new theory on tacit knowledge per se. This is not to say that such work lacks value. Rather the requirements of research validity are not met because it does not truly measure the construct in question.
        </p>
        <p>
          In contrast, knowledge management researchers who take an interpretivist stance accept from the outset that knowledge cannot be studied objectively (<a href="#pan14">for example, Panahi, 2014, p.67</a>). So in their work they deploy qualitative techniques such as interviews, focus groups and surveys, in case study settings (<a href="#hal07b">for example, Hall &amp; Goody, 2007</a>). Such work includes a number of studies that focus on questions related to tacit knowledge (<a href="#mur07">for example, Murray &amp; Peyrefitte, 2007</a>; <a href="#nev03">Neve, 2003</a>; <a href="#why12">Whyte &amp; Classen, 2012</a>). These studies usually do not generate models, but instead provide nuanced understandings of particular aspects of knowledge management. This body of work is subject to common criticisms of qualitative research in the social sciences: for example, claims that limited population sampling results in findings that cannot be generalised, and are therefore not reliable (<a href="#bry12">Bryman, 2012</a>, p. 69-70; <a href="#lec82">LeCompte &amp; Goetz, 1982</a>, p. 35). However, it can be argued that a deep analysis through the generation of a single case study is valuable because it can contribute to a <em>'collective process of knowledge accumulation</em>' (<a href="#fly06">Flyvbjerg, 2006</a>, p. 227). The <em>'power of good example</em>' (<a href="#fly01">Flyvbjerg, 2001</a>, p. 77), where close observation of the object of the study in depth - for example in a single information-rich case adopted for theoretical rather than statistical reasons - has the potential to broaden understanding of a phenomenon. This argument is supported by knowledge management researchers such as Kane <em>et al.</em> (<a href="#kan05">2005</a>, p.147-148) who argue for the use of ethnographic studies in knowledge management research, especially for work that is focused on tacit knowledge sharing. Equally others have pointed to the value of knowledge management studies that collect data over long time periods to generate robust findings (<a href="#mil14b">Milton, 2014</a>; <a href="#ras16">Rasmussen &amp; Hall, 2016</a>, p. 366).&nbsp;
        </p>
        <p>
          A third option open to knowledge management researchers is to adopt a mixed method research design, i.e., one that incorporates both qualitative and quantitative strategies. Seven per cent of studies that consider public sector knowledge management take this approach (<a href="#mas15">Massaro, Dumay &amp; Garlatti, 2015</a>, p. 539). Although it is routinely stated that a mixed methods strategy lends robustness to research, particularly in respect of triangulation, some knowledge management researchers with interests in explorations of tacit knowledge are critical of such claims. Citing Smith (<a href="#smi83">1983</a>), Bryman, (<a href="#bry12">2012</a>, p. 629) points to the different ontological roots of qualitative and quantitative methods and their lack of compatibility. It has also been suggested that those who combine methods are misguided in thinking that this will guarantee the validity and reliability of their research, hoping that it will be recognised as ‘scientific’ to an external audience (<a href="#kan05">Kane <em>et al.</em>, 2005</a>, p.147).
        </p>
        <h2>
          The compromise of a pragmatic approach to explore the intangible
        </h2>
        <p>
          A reading of the general research literature, in combination with that specific to studies in knowledge management where the focus is tacit knowledge, as summarised above would suggest a ‘gold standard’ of deep ethnographic studies conducted over extended time periods by researchers immersed in the environment under scrutiny where they are able to study in situ the information behaviours of data subjects. For full-time doctoral students, however, an obvious challenge of meeting this ideal is the time limit imposed on the period of study, especially since their work is usually carried out (at least in the social sciences) as an independent endeavour as a form of research apprenticeship.
        </p>
        <p>
          Other compromises also need to be made with respect to the object of investigation, for example to access the selected population (in this case employees in a public sector organization) in an appropriate context (the organization itself). Thus in this case a pragmatic interpretivist approach has been determined to allow for an examination of subjective experiences in an organizational context, the key features of which are summarised in Table 1.
        </p>
        <table class="center">
          <caption>
            Table 1: Key features of the research approach
          </caption>
          <tbody>
            <tr>
              <th>
                Feature of research design
              </th>
              <th>
                Decision
              </th>
              <th>
                Justification
              </th>
            </tr>
            <tr>
              <td>
                <strong>Approach</strong>
              </td>
              <td>
                Qualitative
              </td>
              <td>
                · Follows dominant practice of knowledge management research in public sector settings (<a href="#mas15">Massaro, Dumay, Garlatti, 2015, p. 539</a>)<br>
                · Allows for an interpretivist perspective<br>
                · Reflects the philosophical standpoint that knowledge of reality is a social construction<br>
                · Appreciates the standpoints of research participants, and situates these in the organizational landscape
              </td>
            </tr>
            <tr>
              <td>
                <strong>Methods</strong>
              </td>
              <td>
                Mixed
              </td>
              <td>
                · For triangulation purposes (with attention paid to risks identified by <a href="#kan05">Kane <em>et al.,</em> 2005</a>)
              </td>
            </tr>
            <tr>
              <td>
                <strong>Research site</strong>
              </td>
              <td>
                Case study
              </td>
              <td>
                · Follows dominant practice in knowledge management research in public sector settings (<a href="#mas15">Massaro, Dumay, Garlatti, 2015, p. 539</a>)<br>
                · Allows for depth of analysis within a bounded environment of a defined community
              </td>
            </tr>
            <tr>
              <td>
                <strong>Data collection</strong>
              </td>
              <td>
                Four activities
              </td>
              <td>
                · Cross-over online survey to establish features of the participants’ landscape and serve as preface for interviews, e.g. platforms available and how they are used, demographic data to profile the user population<br>
                · Semi-structured interviews to explore individual perspectives, allowing a degree of flexibility on the part of the researcher and interviewees (recruited from survey responses)<br>
                · Focus groups explore group perspectives and validate interpretation of results from analysis of survey and interview data<br>
                · Content analysis of documentation related to the environment under scrutiny (organizational information and details of platform development) to provide complementary contextual information about the implementation of knowledge sharing tools (e.g. social media) within the research setting and triangulate survey, interview and focus group data
              </td>
            </tr>
          </tbody>
        </table>
        <p>
          Thus the methodological approach viewed as most relevant within the frame of this empirical research is one that deploys mixed methods executed as an inductive case study. This will take into account the limitations imposed by the choices adopted, as explored in the literature evaluated above. While there is unavoidable compromise in the implementation of the research, the risk to the integrity of research findings will be minimised.
        </p>
        <p>
          This paper has illustrated the importance of a systematic consideration of the nature of constructs that occupy the core of a doctoral study (in this case tacit knowledge) at the stage of research design. This is important to ambitions for, and claims of, extending theoretical perspectives in the domain in the final output of the work. As such this work intends to open the debate on methodological choice and routes to implementation for studies that are subject to practical constraints imposed by the context in which they are situated.
        </p>
        <h2 id="author">
          About the author
        </h2>
        <p>
          <strong>Iris Buunk</strong> is a doctoral student within the School of Computing at Edinburgh Napier University. She holds an MSc in Information Science from the Haute Ecole de Gestion de Genève and the L'École de Bibliothéconomie et des Sciences de l'Information) at the Université de <em>Montréal,</em> and a Diploma in Librarianship, Documentation and Archives from the École Supérieur d’Information Documentaire, Geneva. She is currently exploring the role, use, and value of social media and tacit knowledge sharing practice in public sector organizations for her doctoral thesis. Iris blogs at https://theknowledgeexplorer.org/, and can be contacted at <a href="mailto:i.buunk@napier.ac.uk">i.buunk@napier.ac.uk</a>. Her mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.<br>
          <strong>Dr Hazel Hall</strong> is Professor and Director of the Centre for Social Informatics within the School of Computing at Edinburgh Napier University. She holds a PhD in Computing from Napier University, an MA in Library and Information Studies from the University of Central England, and a BA (Spec Hons) in French from the University of Birmingham. Her research interests include information sharing in online environments, knowledge management, social computing/media, online communities and collaboration, library and information science research, and research impact. She blogs at http://hazelhall.org and can be contacted at <a href="mailto:h.hall@napier.ac.uk">h.hall@napier.ac.uk</a>. Her mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.<br>
          <strong>Dr Colin F Smith</strong> is Senior Lecturer and Subject Group Leader for Information Systems within the School of Computing at Edinburgh Napier University. He holds a PhD in Management and a Masters degree in Information and Administrative Management from Glasgow Caledonian University, and a BA (Hons) in Politics from the University of Strathclyde. His research interests focus on the relationships between new information and communication technologies, strategic innovation and organizational change. He can be contacted at <a href="mailto:cf.smith@napier.ac.uk">cf.smith@napier.ac.uk</a>. His mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.
        </p>
      </section>
      <section class="refs">
        <h2>
          References
        </h2>
        <ul class="refs">
          <li id="abd09">Abdullah, F., Ingram, A. &amp; Welsh, R. (2009). Managers’ perceptions of tacit knowledge in Edinburgh's Indian restaurants. <em>International Journal of Contemporary Hospitality Management</em>, <em>21</em>(1), 118–127.
          </li>
          <li id="bor13">Borges, R. (2013). Tacit knowledge sharing between IT workers: the role of organizational culture, personality, and social environment. <em>Management Research Review</em>, <em>36</em>(1), 89–108.
          </li>
          <li id="bry12">Bryman, A. (2012) <em>Social research methods</em>. Oxford: Oxford University Press.
          </li>
          <li id="des03">Desouza, K. C. (2003). Facilitating tacit knowledge exchange. <em>Communications of the ACM</em>, <em>46</em>(6), 85–88.
          </li>
          <li id="dur07">Du, R., Ai, S. &amp; Ren, Y. (2007). Relationship between knowledge sharing and performance: a survey in Xi’an, China. <em>Expert Systems with Applications</em>, <em>32</em>(1), 38–46.
          </li>
          <li id="fly01">Flyvbjerg, B. (2001). <em>Making social science matter: why social inquiry fails and how it can succeed again</em>. Oxford, UK: Cambridge University Press.
          </li>
          <li id="fly06">Flyvbjerg, B. (2006). Five misunderstandings about case-study research. <em>Qualitative Inquiry</em>, 12(2), 219–245.
          </li>
          <li id="gar07">Garcia-Perez, A. &amp; Mitra, A. (2007). Tacit knowledge elicitation and measurement in research organizations: a methodological approach<em>. Electronic Journal of Knowledge Management</em>, 5(4), 373–385.
          </li>
          <li id="hal07a">Halbwirth, S.J. &amp; Olsson, M.R. (2007). Working in parallel: themes in knowledge management and information behaviour. In S. Hawamdeh (Ed.) <em>Creating collaborative advantage through knowledge and innovation</em> (pp. 69–89). Singapore: World Scientific.
          </li>
          <li id="hal07b">Hall, H. &amp; Goody, M. (2007). Knowledge management culture and compromise – interventions to promote knowledge sharing supported by technology in corporate environments.&nbsp;<em>Journal of Information Science</em>,&nbsp;33(2), 181-188.
          </li>
          <li id="hen99">Hendriks, P. (1999). Why share knowledge? The influence of ICT on the motivation for knowledge sharing. <em>Knowledge and Process Management</em>, 6(2), 91–100.
          </li>
          <li id="his13">Hislop, D. (2013). <em>Knowledge management in organizations: a critical introduction</em> (3<sup>rd</sup> ed.). New York: Oxford University Press.
          </li>
          <li id="hsu08">Hsu, C. L. &amp; Lin, J. C. C. (2008). Acceptance of blog usage: the roles of technology acceptance, social influence and knowledge sharing motivation. <em>Information and Management</em>, 45(1), 65–74.
          </li>
          <li id="huy06">Huysman, M. &amp; Wulf, V. (2006). IT to support knowledge sharing in communities, towards a social capital analysis. <em>Journal of Information Technology</em>, 21(1), 40–51.
          </li>
          <li id="kan05">Kane, H., Ragsdell, G. &amp; Oppenheim, C. (2005). Knowledge management methodologies. <em>The Electronic Journal of Knowledge Management</em>, 4(2), 141–152.
          </li>
          <li id="lec82">LeCompte, M. D. &amp; Goetz, J. P. (1982). Problems of reliability and validity in ethnographic research. <em>Review of Educational Research</em>, 52(1), 31
          </li>
          <li id="liy10">Li, Y. &amp; Zhang, L. (2010). The knowledge sharing models of knowledge management implemented with multi-agents. In <em>Proceedings of the International Conference on E-product E-Service and E-Entertainment (ICEEE 2010)</em> (pp. 2441-2444). New York: IEEE.
          </li>
          <li id="lin07">Lin, C. P. (2007). To share or not to share: Modeling tacit knowledge sharing, its mediators and antecedents. <em>Journal of Business Ethics</em>, 70(4), 411–428.
          </li>
          <li id="mas15">Massaro, M., Dumay J. &amp; Garlatti, A. (2015). Public sector knowledge management: a structured literature review. <em>Journal of Knowledge Management</em>, 19(3), 530-558.
          </li>
          <li id="mil02">Miller, F. (2002). I= 0-(Information has no intrinsic meaning). Information Research, 0. <em>Information Research</em>, 8(1), 1-14. Retrieved from http://informationr.net/ir/8-1/paper140.html (Archived by WebCite® at http://www.webcitation.org/6htdqDAOe)
          </li>
          <li id="mil14a">Mills, L. A., Knezek, G. &amp; Khaddage, F. (2014). Information seeking, information sharing, and going mobile: three bridges to informal learning. <em>Computers in Human Behavior</em>, 32, 324–334.
          </li>
          <li id="mil14b">Milton, N. (2014). Findings from international surveys providing a snapshot of the state of knowledge management from a practitioner point of view. <em>Journal of Entrepreneurship, Management and Innovation</em> <em>(EMI),</em> 10(1), 109–127.
          </li>
          <li id="mur07">Murray, S. R. &amp; Peyrefitte, J. (2007). Knowledge type and communication media choice in the knowledge transfer process. <em>Journal of Managerial Issues</em>, 19(1), 111–133.
          </li>
          <li id="nev03">Neve, T. O. (2003). Right questions to capture knowledge. <em>Knowledge Creation Diffusion Utilization</em>, 1(1), 47–54.
          </li>
          <li id="nga09">Ngah, R. &amp; Jusoff, K. (2009). Tacit knowledge sharing and SMEs’ organizational performance. <em>International Journal of Economics and Finance</em>, 1(1), 216–220.
          </li>
          <li id="non94">Nonaka, I. (1994). A dynamic theory of organizational knowledge creation. <em>Organization Science</em>, 5(1), 14–37.
          </li>
          <li id="pan13">Panahi, S., Watson, J. &amp; Partridge, H. (2013). Towards tacit knowledge sharing over social web tools. <em>Journal of Knowledge Management</em>, 17(3), 379–397.
          </li>
          <li id="pan14">Panahi, Sirous. (2014). <em>Social media and tacit knowledge sharing: physicians' perspectives and experiences</em>. (Unpublished doctoral thesis). Brisbane: Queensland University of Technology.
          </li>
          <li id="ras16">Rasmussen, L. &amp; Hall, H. (2016). The adoption process in management innovation: a knowledge management case study. <em>Journal of Information Science</em>, <em>42</em>(3), 386-395.
          </li>
          <li id="sal13">Salleh, K., Chong, S.C., Ahmad, S.N.S. &amp; Ikhsan, S.O.S.S. (2013). The extent of influence of learning factors on tacit knowledge sharing among public sector accountants. <em>Vine: The Journal of Information and Knowledge management Systems</em>, 43(4), 424–441.
          </li>
          <li id="scu13">Scully, J. W., Buttigieg, S. C., Fullard, A., Shaw, D. &amp; Gregson, M. (2013). The role of SHRM in turning tacit knowledge into explicit knowledge: a cross-national study of the UK and Malta. <em>The International Journal of Human Resource Management</em>, 24(12), 2299–2320.
          </li>
          <li id="smi83">Smith, J. K. (1983). Quantitative versus qualitative research: an attempt to clarify the issue. <em>Educational Researcher, 12</em>(3), pp. 6-13.
          </li>
          <li id="tsa14">Tsai, A. (2014). An empirical model of four processes for sharing organizational knowledge. <em>Online Information Review</em>, 38(2), 305–320.
          </li>
          <li id="ven12">Venkitachalam, K. &amp; Busch, P. (2012). Tacit knowledge: review and possible research directions. <em>Journal of Knowledge Management</em>, 16(2), 356-371.
          </li>
          <li id="vir10">Virtanen, I. (2010). Epistemological problems concerning explication of tacit knowledge. <em>Journal of Knowledge management Practice</em>, 11(4), 1–17.
          </li>
          <li id="why12">Whyte, G. &amp; Classen, S. (2012). Using storytelling to elicit tacit knowledge from SMEs. <em>Journal of Knowledge Management</em>, 16(6), 950–962.
          </li>
          <li id="wid08">Widen-Wulff, G., Ek, S., Ginman, M., Perttila, R., Sodergard, P. &amp; Totterman, A.-K. (2008). Information behaviour meets social capital: a conceptual model. <em>Journal of Information Science,</em> 34(3), 346–355.
          </li>
        </ul>
        <h3 style="text-align:center;">
          How to cite this paper
        </h3>
        <div class="citing">
          Buunk, I., Hall, H. &amp; Smith, C. (2017). Tacit knowledge sharing and social media: the determination of a methodological approach to explore the intangible In <em>Proceedings of ISIC, the Information Behaviour Conference, Zadar, Croatia, 20-23 September, 2016: Part 2</em>. <em>Information Research, 22</em>(1), paper isics1606). Retrieved from http://InformationR.net/ir/22-1/isic/isics1606.html (Archived by WebCite® at http://www.webcitation.org/6oW4iu6mu)
        </div>
      </section>
    </article><br>
    <section>
      
      <br>
      
      
      <hr>
      
    </section>
    <footer>
      <hr>
      
      <hr>
    </footer><!-- Go to www.addthis.com/dashboard to customize your tools -->
  </body>
</html>
